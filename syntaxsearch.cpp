#include <iostream>

#include "clang/AST/ASTContext.h"
#include "clang/ASTMatchers/ASTMatchers.h"
#include "clang/ASTMatchers/ASTMatchFinder.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"

using namespace clang::tooling;
using namespace llvm;
using namespace clang;
using namespace clang::ast_matchers;

static llvm::cl::OptionCategory MyToolCategory("my-tool options");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
static cl::extrahelp MoreHelp("\nMore help text...");

auto matcher = functionDecl(hasDescendant(varDecl(hasName("dummy")))).bind("stmt");

class MatchPrinter : public MatchFinder::MatchCallback {
  public:
    virtual void run (const MatchFinder::MatchResult &Result) {
      ASTContext* ctx = Result.Context;
      const Decl* d = Result.Nodes.getNodeAs<clang::Decl>("stmt");
      //d->dump();

      std::cout << "decl " << d->getLocStart().printToString(ctx->getSourceManager()) << std::endl;
      FullSourceLoc l = ctx->getFullLoc(d->getLocStart());
      if (l.isValid()) {
        std::cout << l.getSpellingLineNumber() << ":" << l.getSpellingColumnNumber() << std::endl;
        const FileEntry* f = ctx->getSourceManager().getFileEntryForID(l.getFileID());
        if (f)
          std::cout << f->getName() << std::endl;
      }
    }
};

int main (int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);
  ClangTool Tool(
    OptionsParser.getCompilations(),
    OptionsParser.getSourcePathList());

  MatchPrinter printer;
  MatchFinder finder;
  finder.addMatcher(matcher, &printer);

  //return Tool.run(newFrontendActionFactory<clang::SyntaxOnlyAction>().get());
  return Tool.run(newFrontendActionFactory(&finder).get());
}

